(function ($) {
  'use strict';
  Drupal.behaviors.votingBehavior = {
    attach: function (context, settings) {

      var storage = localStorage;
      $('.rate-field-container .up', context)
        .once('clickVote')
        .click(function () {

           if(drupalSettings.user.uid == 0){
               window.location.href = '/rate_field/redirect_anon';
           }else{
             setUpVote(this);
           }
        });
      $('.rate-field-container .down', context)
        .once('clickVote')
        .click(function () {

           if(drupalSettings.user.uid == 0){
               window.location.href = '/rate_field/redirect_anon';
           }else{
             setDownVote(this);
           }
        });

      function setNewVotesAttributes(upVotes, downVotes, thumbs_elemnt) {
        var element = $(thumbs_elemnt);
        var vote_element = element.parent();
        var percentage = 0;
        if (upVotes >= 0 && downVotes >= 0) {
          percentage = (upVotes + downVotes === 0) ? 100 : parseInt((upVotes) / (upVotes + downVotes) * 100);
          $('.info-data', vote_element)
            .text(percentage + '%' + ' (' + upVotes + '/' + downVotes + ')');
          $('.visual-bar .green', vote_element).css('width', percentage + '%');
          $('.visual-bar .red', vote_element)
            .css('width', (100 - percentage) + '%');
          vote_element.attr('up_votes', upVotes);
          vote_element.attr('down_votes', downVotes);
        }
      }

      function setUpVote(votingElement) {
        var vote_element = $(votingElement).parent();
        var up_votes = parseInt(vote_element.attr('up_votes'));
        var down_votes = parseInt(vote_element.attr('down_votes'));
        var field_name = vote_element.attr('field_name');
        var up_context = $(votingElement);
        var down_context = $('.down', vote_element);
        var entity_type = vote_element.attr('entity_type');
        var entity_id = vote_element.attr('entity_id');
        if (up_context.hasClass('tunv')) {
          storage.setItem(field_name + ':' + entity_type + ':' + entity_id, 'UP');
          $.ajax('/rate_field/' + entity_type + '/' + 'up' + '/' + field_name + '/' + entity_id + '  ');
          if (down_context.hasClass('tdv')) {
            setNewVotesAttributes(Math.max(0, up_votes + 1), Math.max(0, down_votes - 1), votingElement);
          }
          else {
            setNewVotesAttributes(Math.max(0, up_votes + 1), Math.max(0, down_votes), votingElement);
          }
        }
        else {
          storage.removeItem(field_name + ':' + entity_type + ':' + entity_id);
          $.ajax('/rate_field/undo/' + entity_type + '/' + field_name + '/' + entity_id + '  ');
          setNewVotesAttributes(up_votes - 1, down_votes, votingElement);
        }
        up_context.toggleClass('tunv');
        up_context.toggleClass('tuv');
        down_context.removeClass('tdv');
        down_context.addClass('tdnv')
      }

      function setDownVote(votingElement) {
        var vote_element = $(votingElement).parent();
        var up_votes = parseInt(vote_element.attr('up_votes'));
        var down_votes = parseInt(vote_element.attr('down_votes'));
        var field_name = vote_element.attr('field_name');
        var down_context = $(votingElement);
        var up_context = $('.up', vote_element);
        var entity_type = vote_element.attr('entity_type');
        var entity_id = vote_element.attr('entity_id');

        if (down_context.hasClass('tdnv')) {
          storage.setItem(field_name + ':' + entity_type + ':' + entity_id, 'DOWN');
          $.ajax('/rate_field/' + entity_type + '/' + 'down' + '/' + field_name + '/' + entity_id + '  ');
          if (up_context.hasClass('tuv')) {
            setNewVotesAttributes(Math.max(0, up_votes - 1), Math.max(0, down_votes + 1), votingElement);
          }
          else {
            setNewVotesAttributes(Math.max(0, up_votes), Math.max(0, down_votes + 1), votingElement);
          }
        }
        else {
          storage.removeItem(field_name + ':' + entity_type + ':' + entity_id);
          $.ajax('/rate_field/undo/' + entity_type + '/' + field_name + '/' + entity_id + '  ');
          setNewVotesAttributes(up_votes, down_votes - 1, votingElement);
        }
        down_context.toggleClass('tdnv');
        down_context.toggleClass('tdv');
        up_context.removeClass('tuv');
        up_context.addClass('tunv')
      }
    }
  };
}(jQuery));