<?php

/**
 * @file
 * Manage ajax requests
 */

namespace Drupal\rate_field\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Rate routes.
 */
class RateFieldController {

  public function redirectAnon() {
    drupal_set_message(t('Please login or register to vote'));
    return new RedirectResponse('/user');
  }


  /**
   * Voting mechanisms
   */
  public function vote($entity_type, $vote_type, $field_name, $entity_id, Request $request) {
    if ($vote_type != 'up' && $vote_type != 'down') {
      return new JsonResponse(array('error'));
    }
    $user_id = \Drupal::currentUser()->id();
    $ip = \Drupal::request()->getClientIp();
    $date = time();
    $vote_value = $vote_type == 'up' ? 1 : -1;
    $this->undo($entity_type, $field_name, $entity_id, $user_id, $ip);
    $vote_array = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'value' => $vote_value,
      'timestamp' => $date,
      'ip' => $ip
    );
    $vote = \Drupal::entityTypeManager()
      ->getStorage('rate_field_vote')
      ->create($vote_array);
    $vote->save();
    $this->calculateResults($entity_type, $field_name, $entity_id);
    return new JsonResponse(array('success'));
  }

  /**
   * Calculate results after each vote
   */
  public function calculateResults($entity_type, $field_name, $entity_id) {
    $db = \Drupal::database();
    $up_data = $db->select('rate_field_vote', 't')
      ->condition('t.value', '1', '=')
      ->condition('t.entity_type', $entity_type, '=')
      ->condition('t.entity_id', $entity_id, '=')
      ->condition('t.field_name', $field_name, '=')
      ->countQuery()
      ->execute()
      ->fetchField();
    $down_data = $db->select('rate_field_vote', 't')
      ->condition('t.value', '-1', '=')
      ->condition('t.entity_type', $entity_type, '=')
      ->condition('t.entity_id', $entity_id, '=')
      ->condition('t.field_name', $field_name, '=')
      ->countQuery()
      ->execute()
      ->fetchField();
    $percentage = 0;
    if (($up_data + $down_data) != 0) {
      $percentage = intval(($up_data) / ($up_data + $down_data) * 100);
    }
    $percentage = "$percentage%";
    $search_array = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    );
    // Load entities by their property values.
    $entities = \Drupal::entityTypeManager()
      ->getStorage('rate_field_vote_result')
      ->loadByProperties($search_array);
    foreach ($entities as $entity) {
      $entity->delete();
    }
    $vote_result_array = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'total_up' => $up_data,
      'total_down' => $down_data,
      'percentage' => $percentage,
      'timestamp' => time()
    );
    $vote = \Drupal::entityTypeManager()
      ->getStorage('rate_field_vote_result')
      ->create($vote_result_array);
    $vote->save();
  }

  /**
   * Undo request handler
   */
  public function undoVote($entity_type, $field_name, $entity_id, Request $request) {
    $user_id = \Drupal::currentUser()->id();
    $ip = \Drupal::request()->getClientIp();
    $this->undo($entity_type, $field_name, $entity_id, $user_id, $ip);
    $this->calculateResults($entity_type, $field_name, $entity_id);
    return new JsonResponse(array('success'));
  }

  /**
   * Undo internal mechanisms
   */
  public function undo($entity_type, $field_name, $entity_id, $user_id, $ip) {
    //Invalidate cache tags
    \Drupal\Core\Cache\Cache::invalidateTags(array('rate_field:' . $field_name . ':' . $entity_type . ':' . $entity_id));
    $all_entities = NULL;
    if (!($cache = \Drupal::cache()->get('all_entities'))) {
      $all_entities = \Drupal::entityTypeManager()->getDefinitions();
      $entity_keys = array_keys($all_entities);
      \Drupal::cache()
        ->set('all_entities', $entity_keys, CacheBackendInterface::CACHE_PERMANENT);
    }
    else {
      $entity_keys = $cache->data;
    }
    if (!array_search($entity_type, $entity_keys)) {
      return new JsonResponse(array('error'));
    }
    $search_array = NULL;
    if ($user_id == 0) {
      $search_array = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'ip' => $ip,
        'uid' => 0
      );
    }
    else {
      $search_array = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'uid' => $user_id
      );
    }

    // Load entities by their property values.
    $entities = \Drupal::entityTypeManager()
      ->getStorage('rate_field_vote')
      ->loadByProperties($search_array);
    foreach ($entities as $entity) {
      $entity->delete();
    }
  }
}
