<?php

/**
 * @file
 * Contains Drupal\rate_field\Entity\RateFieldVoteResult.
 */

namespace Drupal\rate_field\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the VoteResult entity.
 *
 * @ingroup votingapi
 *
 * @ContentEntityType(
 *   id = "rate_field_vote_result",
 *   label = @Translation("Rate Field Vote Result"),
 *   base_table = "rate_field_vote_result",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class RateFieldVoteResult extends ContentEntityBase implements ContentEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getVotedEntityType() {
    return $this->get('entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVotedEntityType($name) {
    return $this->set('entity_type', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getVotedEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVotedEntityId($id) {
    return $this->set('entity_id', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalUp() {
    return $this->get('total_up')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotalUp($value) {
    return $this->set('total_up', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalDown() {
    return $this->get('total_down')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTotalDown($value) {
    return $this->set('total_down', $value);
  }


  /**
   * {@inheritdoc}
   */
  public function getPercentage() {
    return $this->get('percentage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPercentage($value) {
    return $this->set('percentage', $value);
  }


  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('timestamp')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    return $this->set('timestamp', $timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName() {
    return $this->get('field_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldName($name) {
    return $this->set('field_name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the RateField Vote Result entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The vote UUID.'))
      ->setReadOnly(TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The type from the voted entity.'))
      ->setSettings(array(
        'max_length' => 64
      ))
      ->setRequired(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Voted entity'))
      ->setDescription(t('The ID from the voted entity'))
      ->setRequired(TRUE);

    $fields['total_up'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Value'))
      ->setDescription(t('The numeric value of the total up votes.'))
      ->setRequired(TRUE);

    $fields['total_down'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Value'))
      ->setDescription(t('The numeric value of the vote.'))
      ->setRequired(TRUE);

    $fields['percentage'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setDescription(t('The numeric value of the percentage'))
      ->setRequired(TRUE);

    $fields['timestamp'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setRequired(TRUE);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field Name'))
      ->setDescription(t('The name of the field'))
      ->setSettings(array(
        'max_length' => 64
      ))
      ->setRequired(TRUE);
    return $fields;
  }
}