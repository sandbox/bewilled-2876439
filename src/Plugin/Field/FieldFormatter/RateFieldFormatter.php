<?php
/**
 * @file
 * Contains Drupal\rate_field\Entity\RateFieldVoteResult.
 */

namespace Drupal\rate_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'videojs_player_list' formatter.
 *
 * @FieldFormatter(
 *   id = "rate_field_formatter",
 *   label = @Translation("Rate Field Formatter"),
 *   field_types = {
 *     "rate_field"
 *   }
 * )
 */
class RateFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = t('Displays the rate field widget');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    foreach ($items as $delta => $item) {
      $field_name = $items->getFieldDefinition()->getName();
      $entity = $items->getEntity();
      $entity_type = $entity->getEntityType()->id();
      $entity_id = $entity->id();
      $user_id = \Drupal::currentUser()->id();
      $vote_value = "";
      if ($user_id != 0) {
        $search_array = array(
          'field_name'=> $field_name,
          'entity_type' => $entity_type,
          'entity_id' => $entity_id,
          'uid' => $user_id
        );
        $vote_entities = \Drupal::entityTypeManager()
          ->getStorage('rate_field_vote')
          ->loadByProperties($search_array);
        $user_vote = reset($vote_entities);
        $vote_value = !empty($user_vote) ? $user_vote->getValue() : "";
      }
      $voted_up = $vote_value == 1 ? "tuv" : "tunv";
      $voted_down = $vote_value == -1 ? "tdv" : "tdnv";
      $search_array = array(
        'field_name'=> $field_name,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
      );
      $vote_results = \Drupal::entityTypeManager()
        ->getStorage('rate_field_vote_result')
        ->loadByProperties($search_array);
      $total_up = 0;
      $total_down = 0;
      $percentage = "100%";
      $user_vote_results = reset($vote_results);
      if (!empty($user_vote_results)) {
        $total_up = $user_vote_results->getTotalUp();
        $total_down = $user_vote_results->getTotalDown();
        $percentage = $user_vote_results->getPercentage();
      }
      $percentage_styles_green = 100;
      $percentage_styles_red = 0;
      if (($total_up + $total_down) != 0) {
        $percentage_styles_green = intval(($total_up) / ($total_up + $total_down) * 100);
        $percentage_styles_red = 100 - $percentage_styles_green;
      }
      $percentage_styles_green =  "$percentage_styles_green%";
      $percentage_styles_red =  "$percentage_styles_red%";
      $elements[$delta] = array(
        '#theme' => 'rate_formatter',
        '#field_name' => $field_name,
        '#up_votes' => $total_up,
        '#down_votes' => $total_down,
        '#entity_type' => $entity_type,
        '#entity_id' => $entity_id,
        '#total_up' => $total_up,
        "#total_down" => $total_down,
        "#percentage" => $percentage,
        "#voted_up" => $voted_up,
        "#voted_down" => $voted_down,
        "#psg" => $percentage_styles_green,
        "#psr" => $percentage_styles_red,
        '#attached' => array(
          'library' => array('rate_field/rate_field'),
        ),
        '#cache' => [
          'tags' => ['rate_field:' . $field_name . ':' . $entity_type . ':' . $entity_id],
          'max-age' => \Drupal\Core\Cache\Cache::PERMANENT,
        ],
      );
    }
    return $elements;
  }
}
