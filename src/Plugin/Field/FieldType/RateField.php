<?php
/**
 * @file
 * Contains Drupal\rate_field\Entity\RateFieldVoteResult.
 */

namespace Drupal\rate_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of Rate Field.
 *
 * @FieldType(
 *   id = "rate_field",
 *   label = @Translation("Rate"),
 *   default_formatter = "rate_field_formatter",
 *   default_widget = "rate_field_widget",
 * )
 */
class RateField extends FieldItemBase  {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'enabled' => array(
          'type' => 'int',
          'size' => 'normal',
          'not null' => FALSE,
        )
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('enabled')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('Up votings'))->setReadOnly(true);

    return $properties;
  }
}
