<?php
/**
 * @file
 * Contains Drupal\rate_field\Entity\RateFieldVoteResult.
 */

namespace Drupal\rate_field\Plugin\Field\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;


/**
 * A widget bar.
 *
 * @FieldWidget(
 *   id = "rate_field_widget",
 *   label = @Translation("Rate field widget"),
 *   field_types = {
 *     "rate_field"
 *   }
 * )
 */

class RateFieldWidget extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $enabled = array(
      '#disabled' => true,
      '#title' => 'Rate Field Statistics',
      '#required' => false,
      '#type' => 'textfield',
      '#default_value' => "This is an empty field for the Rate Field module. Please do not interact with this value",
      '#size' => 70,
      '#maxlength' => 70,
      '#element_validate' => array(
        array($this, 'validate'),
      ),
    );
    return array('temp' => $enabled);
  }

  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
  }
}
